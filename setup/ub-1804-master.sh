#!/bin/bash

POD_NETWORK_CIDR="10.244.0.0/16"
SERVICE_CIDR="10.96.0.0/12"

# initialise Kube

kubeadm init --pod-network-cidr=$POD_NETWORK_CIDR --service-cidr=$SERVICE_CIDR

# install Flannel pod network
wget https://bitbucket.org/sagittarius_rbrisley/k/raw/master/setup/flannel/kube-flannel.yml

kubectl --kubeconfig='/etc/kubernetes/admin.conf' apply -f kube-flannel.yml